import fetch from 'node-fetch'
import RssParse from '../libs/rss-parse/rss-parse'

export function service(evt, ctx, cb) {

  //Fetch URL stored as a Lambda environment variable
  fetch(process.env.RSS_FEED_URL,  {
  	method: 'get',
    mode: 'cors'
  }).then(resp => {
    return resp.text()
  }).then(text => {
    console.log(text)
    return RssParse(text)
      .itemSet('item') //Sets the feed items seperator
      .contentSet(['title', 'link']) //Sets the feed elements to return
      .clearCdata('title') //Clears any CDATA information for the selected element
      .linksSet(process.env.LINK_QUERY) //Image link query stored as an environment variable
      .contentGet() //returns parsed feed
  }).then(data => {
    ctx.done(null, data)
  }).catch(err => {
    ctx.done(err)
  })
}
