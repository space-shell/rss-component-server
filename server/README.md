# RSS Component Server - API

[![pipeline status](https://gitlab.com/spaceshell_j/rss-component-server/badges/master/pipeline.svg)](https://gitlab.com/spaceshell_j/rss-component-server/commits/master)

This package should be used as a template for deploying an RSS Parser API as an AWS Lambda function. The function reads an RSS feed in XML format and returns a JSON object containing specified feed elements.

## Table of Contents
1. [Requirements](#requirements)
1. [Installation](#getting-started)
1. [Running the Project](#running-the-project)
1. [Project Structure](#project-structure)
1. [Testing](#testing)
1. [Deployment](#deployment)

## Requirements
* Node `^5.0.0`
* NPM `^3.0.0`
* [AWS CLI](#https://aws.amazon.com/cli/?sc_channel=PS&sc_campaign=Acquisition_UK&sc_publisher=google&sc_medium=command_line_b&sc_content=aws_cli_e&sc_detail=aws%20cli&sc_category=command_line&sc_segment=211476221648&sc_matchtype=e&sc_Country=UK&s_kwcid=AL!4422!3!211476221648!e!!g!!aws%20cli&ef_id=WfkThAAABQkoOf@x:20171101002124:s)

After installing the requirements it is necessary to configure the AWS CLI to your AWS account.

```bash
$ aws configure
```

You will be prompted to enter your `Access Key ID` as well as your `Secret Access Key` and the region for the AWS account you are working from.

## Installation

After confirming that your environment meets the above [requirements](#requirements), you can create a new project by
doing the following:

```bash
$ git clone https://gitlab.com/spaceshell_j/rss-component-server.git <my-project-name>
$ cd <my-project-name>
$ npm install
```

Once the packages have been installed it is necessary to create a new file in the root directory `serverless.env.yml` to store your RSS feed URL and search parameters as AWS Lambda environment variables. The file should contain the following:

```yaml
dev:
  RSS_FEED_URL: #RSS Feed URL
  LINK_QUERY: #Links Search Query
```

## Running the Project

After completing the [installation](#installation) step, you're ready to start the project!

Before running the project it may be necessary to set the RSS feed parsing parameters, these are set within the `rss-scraper.js` in the `src` directory. The following lines of code are used to set the RSS parsing parameters.

```javascript
const rssParse = RssParse(data) //Sets XML data
  .itemSet('item') //Sets the feed items seperator
  .contentSet(['title', 'link']) //Sets the feed elements to return
  .clearCdata('title') //Clears any CDATA information for the selected element
  .linksSet('url=') //Image link query stored as an environment variable
  .contentGet() //returns parsed feed
```

Whilst in development there are a few additional scripts at your disposal:

|`npm <script>`|Description|
|-|-|
|`test`|Runs unit tests with Jest. See [testing](#testing)|
|`invoke:dev`|Runs the Lambda function locally|
|`deploy:dev`|Deploys function to AWS (Dev)|



>Note: You must set your AWS CLI credentials as secret environment variables within your GitLab CI / CD pipeline settings in order to deploy your function autonomously.

```bash
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
```


## Project Structure

The project structure presented in this template is **fractal**, where functionality is grouped primarily by feature rather than file type.

|`Key Files`     |Description|
|-|-|
|`.gitlab-ci.yml`|Runs GitLab Pipeline Commands for CI / CD|
|`serverless.yml`|Serverless config file for the AWS Lambda function parameters|
|`src\rss-scraper.js`|Main Lambda function file|
|`libs\rss-parse\rss-parse.js`|RSS Parser module|

## Testing
To add a unit test, create a `.test.js` file anywhere inside of `src` or `libs`. Jest will automatically find these files and test them using the `npm test` command or via pipeline commands.
