import React from 'react'
import ReactDOM from 'react-dom'
import RssApp from './pages/rss-app'

import './index.css';
import './atomic.css';

ReactDOM.render(
  <RssApp url="https://z1c9xygpjf.execute-api.eu-west-1.amazonaws.com/dev/api" />,
  document.getElementById('root')
)
