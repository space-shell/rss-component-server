import React, { Component } from 'react';
import FeedItem from '../../modules/feed-item'

export default class RssApp extends Component {
	state = {
    url: this.props.url
  }

  //Fetch RSS JSON and set to state
  componentDidMount(){
    fetch(this.state.url, {
    	method: 'get',
      mode: 'cors'
    }).then(resp => {
      return resp.json()
    }).then(data => {
      this.setState({
        items: [...data]
      })
    }).catch(err => {
			console.error(err)
			this.setState({
        error: err
      })
		})
  }

	render() {
		return (
      <div className="App">
  			{ !!this.state.items ? (
          this.state.items.map((item, index) => {
            return <FeedItem key={`FeedItem-${index}`} title={item.title} links={item.links} />
          })
        ) : (
          <div>
            {this.state.error}
  			  </div>
        )}
      </div>
		)
	}
}
