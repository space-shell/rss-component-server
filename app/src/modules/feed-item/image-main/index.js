import React from'react';

const ImageMain = ({ source }) => (
	<div className="H(100%)">
    { !!source ? (
			<img src={source} alt="Main" className="Maw(100%) H(100%)"/>
		):(
			<div className="D(n)">No images Here</div>
		)}
	</div>
)

export default ImageMain
