import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import FeedItem from './'
import ImageMain from './image-main'
import ImageMinor from './image-minor'

import 'jest-enzyme'

Enzyme.configure({ adapter: new Adapter() })

const data = [
	"a150b45259fac13dedb5b77c3a26a8dd.png",
	"2c9d844859fa4169edb5246bf66d9cd4.jpg",
	"fb7afe3059fac13dedb58b5ccaca7ead.png",
	"bb9b284459fac13dedb5ad051201fbf2.jpg"
]

const minor = [
	"2c9d844859fa4169edb5246bf66d9cd4.jpg",
	"fb7afe3059fac13dedb58b5ccaca7ead.png",
	"bb9b284459fac13dedb5ad051201fbf2.jpg"
]

const title = "Awesome title"

describe('Testing RSS feed item', () => {
	const module = shallow(<FeedItem title={title} links={data} />)
	const module_blank = shallow(<FeedItem title={undefined} links={data}/>)

	it('Passes 1st image as Hero', () => {
		expect(module).toContainReact(<ImageMain source={data[0]}/>)
	})

	it('Does not display error message', () => {
		expect(module.find('h1')).not.toBePresent()
	})

	it('Does error message', () => {
		expect(module_blank.find('h1')).toBePresent()
	})

	it('Passes props', () => {
		expect(module.find('ImageMain')).toHaveProp('source')
		expect(module.find('ImageMinor')).toHaveProp('sources')
	})

	it('Passes title test to header', () => {
		expect(module.find('h2')).toHaveText(title)
	})
})

describe('Testing Image Minor Component', () => {
	const module = shallow(<ImageMinor sources={minor} />)
	const module_blank = shallow(<ImageMinor sources={[]} />)

	it('Renders Minor images', () => {
		expect(module.find('img').length).toBe(minor.length)
	})
})
