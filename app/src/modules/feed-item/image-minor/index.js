import React from'react';

const ImageMinor = ({ sources }) => (
	<div className="H(50%)">
		{sources.map((source, idx) => {
			return (<img
				key={`ImageMinor-${idx}`}
				src={source}
				alt="Minor"
				className="Maw(100%) H(50%)"
			/>)
		})}
	</div>
)

export default ImageMinor;
