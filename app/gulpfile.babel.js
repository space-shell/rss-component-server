import gulp from 'gulp'
import pump from 'pump'
import atomizer from 'gulp-atomizer'

var paths = {
  watch: ['src/**/*.js', '!src/**/*.test.js'],
  js: ['src/**/*.js', '!src/**/*.test.js']
};

// Generates `atomic.css` file from inline styles
gulp.task('atomize', cb => {
  pump([
    gulp.src(paths.js),
    atomizer({
      outfile: 'atomic.css'
    }),
    gulp.dest('src')
  ], cb)
})

gulp.task('watch', () => {
  gulp.watch(paths.watch, ['atomize'])
})
